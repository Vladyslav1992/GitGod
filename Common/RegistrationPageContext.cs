using System.Collections.Generic;
using Automation.Common.DataModels.Enums;
using Automation.Common.DataModels.Users;
using Automation.Common.Helpers.Extensions;
using Automation.Common.Pages.RegistrationPg;
using SeleniumExtras.PageObjects;
using Wait = Automation.Common.Helpers.Wait;

namespace Automation.Common.Contexts
{
    public class RegistrationPageContext : CommonContext
    {
        public RegistrationPage RegistrationPage;

        public RegistrationPageContext()
        {
            RegistrationPage = new RegistrationPage()
            PageFactory.InitElements(Driver.DriverInstanse, RegistrationPage);
        }

        public override string GetPageUrl() 
            => SitePages.Registration.GetEnumDescription();

        public override void WaitForLoad()
        {
            var isLoaded = RegistrationPage.IsLoaded();
            Wait.For(() => isLoaded);
        }

        public RegistrationPageContext Navigate()
        {
            NavigateToPage(GetPageUrl());
            WaitForLoad();
            return this;
        }

        public bool IsRegistered()
        {
            return RegistrationPage.IsRegistered();
        }

        public void RegisterAs(User user)
        {
            RegistrationPage.FillAndSubmit(user);
        }

        public void RegisterAs(string email, string fullName, string password, string cpassword)
        {
            RegistrationPage.FillAndSubmit(email, fullName, password, cpassword);
        }

        public void FillRegistrationForm(User user)
        {
            RegistrationPage.FillForm(user);
        }

        public void FillRegistrationForm(string email, string fullName, string password, string cpassword)
        {
            RegistrationPage.FillForm(email, fullName, password, cpassword);
        }

        public void Submit()
        {
            RegistrationPage.Submit();
        }

        public string GetUserRegistered()
            => RegistrationPage.GetUser();

        public IEnumerable<string> GetErrorMessages()
            => RegistrationPage.GetErrors();
    }
}
